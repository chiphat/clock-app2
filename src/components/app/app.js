import React, {Component} from 'react';
import Clock from '../clock/clock.js'

export default class App extends Component {
    render() {
        return (
            <div className="app">
                <Clock />
            </div>
        );
    }
}
