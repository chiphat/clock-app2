import './clock.less';
import React, {Component} from 'react';
import moment from 'moment';

export default class Clock extends Component {
    constructor(props) {
        super(props);

        function getAnimationDelay(seconds) {
            return {
                WebkitAnimationDelay: '-' + seconds + 's'
            };
        }

        var now = new Date();
        var seconds = now.getSeconds();
        var minutes = now.getMinutes() + seconds / 60;
        var hours = now.getHours() + minutes / 60;

        this.state = {
            now: now,
            secondDelay: getAnimationDelay(seconds),
            minuteDelay: getAnimationDelay(minutes * 60),
            hourDelay: getAnimationDelay(hours * 60 * 60)
        };
    }

    render() {
        var time = moment(this.state.now);

        return <div className="clock">
            <svg className="animated-clock" width="100" height="100" fill="transparent" viewBox="0 0 100 100">
                <circle cx="50" cy="50" r="45"/>
                <path className="hours hand" d="M50,50L50,30" style={this.state.hourDelay} />
                <path className="minutes hand" d="M50,50L50,20" style={this.state.minuteDelay} />
                <path className="seconds hand" d="M50,50L50,15" style={this.state.secondDelay} />
                <circle className="dot" cx="50" cy="50" r="1"/>
            </svg>
            <div className="date">{time.format('Do of MMMM')}</div>
            <div className="date">{time.format('Do of YYYY')}</div>
        </div>;

    }
}