import './main.less';
import App from './components/app/app.js';
import {render} from 'react-dom';
import React from 'react';

render(<App/>, document.getElementById('root'));