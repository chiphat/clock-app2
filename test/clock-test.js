import React from 'react';
import assert from 'assert';
import {createRenderer} from 'react-addons-test-utils';
import {expect} from 'chai';
import Clock from '../src/components/clock/clock.js';

describe('Clock component', () =>  {
    var clockEl;

    beforeEach(() => {
        const renderer = createRenderer();
        renderer.render(<Clock/>);
        clockEl = renderer.getRenderOutput();
        var asd;
    });

    it('<Clock> should be of type <div>', function() {
        expect(clockEl.type).to.equal('div');
    });

    it('<Clock> should contain <svg> and <div> children', function() {
        expect(clockEl.props.children.length).to.equal(3);
    });

    it('<Clock> should contain animated clock', function() {
        var animatedClock = clockEl.props.children[0];
        expect(animatedClock.props.className).to.equal('animated-clock');
    });

    it('<Clock> should contain date', function() {
        var date = clockEl.props.children[1];
        expect(date.props.className).to.equal('date');
    });
});